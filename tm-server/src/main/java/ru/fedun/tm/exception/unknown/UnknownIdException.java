package ru.fedun.tm.exception.unknown;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class UnknownIdException extends AbstractRuntimeException {

    private final static String message = "Error! Unknown id...";

    public UnknownIdException() {
        super(message);
    }

}
