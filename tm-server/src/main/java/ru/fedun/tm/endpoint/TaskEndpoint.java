package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.ITaskEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint implements ITaskEndpoint {

    private ServiceLocator serviceLocator;

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "id") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> showAllTasks(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Task showTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getOneById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task showTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getOneByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task showTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getOneByTitle(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByTitle(session.getUserId(), name);
    }

}
