package ru.fedun.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractEntity implements Serializable {

    @NotNull
    private String title;

    @NotNull
    private String description;

    @Nullable
    private String userId;

    @Override
    public String toString() {
        return getTitle();
    }

}
