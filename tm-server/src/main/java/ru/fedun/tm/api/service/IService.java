package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IService<E> {

    @NotNull
    List<E> findAll();

    void clear();

    void load(@NotNull final List<E> es);

    void load(@NotNull final E... es);

    List<E> getEntity();
}
