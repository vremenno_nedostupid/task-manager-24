package ru.fedun.tm.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.exception.unknown.UnknownIdException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.util.SignatureUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class SessionRepositoryTest {

    final SessionRepository sessionRepository = new SessionRepository();

    @Before
    public void init() {
        final Session session1 = new Session();
        session1.setStartTime(System.currentTimeMillis());
        session1.setUserId("001");
        session1.setSignature(SignatureUtil.sign(session1, "qwe", 5));

        final Session session2 = new Session();
        session2.setStartTime(System.currentTimeMillis());
        session2.setUserId("001");
        session2.setSignature(SignatureUtil.sign(session1, "rty", 15));

        final Session session3 = new Session();
        session3.setStartTime(System.currentTimeMillis());
        session3.setUserId("001");
        session3.setSignature(SignatureUtil.sign(session1, "uio", 53));

        final Session session4 = new Session();
        session4.setStartTime(System.currentTimeMillis());
        session4.setUserId("002");
        session4.setSignature(SignatureUtil.sign(session1, "asd", 7));

        sessionRepository.add(session1, session2, session3, session4);
    }

    @Test
    public void findAllByUserIdTest() {
        assertEquals(sessionRepository.findAllSessions("001").size(), 3);
    }

    @Test
    public void findByUserIdTest() {
        final Session session = sessionRepository.getEntities().get(3);
        final String userId = session.getUserId();
        assertEquals(sessionRepository.findByUserId(userId), session);
    }

    @Test
    public void removeByUserIdTest() {
        final Session session = sessionRepository.getEntities().get(3);
        sessionRepository.removeByUserId(session.getUserId());
        assertFalse(sessionRepository.getEntities().contains(session));
    }

    @Test
    public void findByIdTest() {
        final Session session = sessionRepository.getEntities().get(3);
        final String id = session.getId();
        assertEquals(sessionRepository.findById(id), session);
    }

    @Test
    public void containsTest() {
        final Session session = sessionRepository.getEntities().get(3);
        final String id = session.getId();
        assertTrue(sessionRepository.contains(id));
    }

    @Test
    public void clearRecordsTest() {
        assertEquals(sessionRepository.getEntities().size(), 4);
        sessionRepository.clear();
        assertEquals(sessionRepository.getEntities().size(), 0);
    }

    @Test
    public void addOneTest() {
        sessionRepository.clear();
        assertEquals(sessionRepository.getEntities().size(), 0);
        final Session session = new Session();
        sessionRepository.add(session);
        assertEquals(sessionRepository.getEntities().size(), 1);
    }

    @Test
    public void addVarargsTest() {
        sessionRepository.clear();
        assertEquals(sessionRepository.getEntities().size(), 0);
        final Session session1 = new Session();
        final Session session2 = new Session();
        final Session session3 = new Session();
        sessionRepository.add(session1, session2, session3);
        assertEquals(sessionRepository.getEntities().size(), 3);
    }

    @Test
    public void addListTest() {
        sessionRepository.clear();
        assertEquals(sessionRepository.getEntities().size(), 0);
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        final Session session3 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessions.add(session3);
        sessionRepository.add(sessions);
        assertEquals(sessionRepository.getEntities().size(), 3);
    }

    @Test
    public void loadOneTest() {
        assertEquals(sessionRepository.getEntities().size(), 4);
        final Session session = new Session();
        sessionRepository.load(session);
        assertEquals(sessionRepository.getEntities().size(), 1);
    }

    @Test
    public void loadVarargs() {
        assertEquals(sessionRepository.getEntities().size(), 4);
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessionRepository.load(session1, session2);
        assertEquals(sessionRepository.getEntities().size(), 2);
    }

    @Test
    public void loadListTest() {
        assertEquals(sessionRepository.getEntities().size(), 4);
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionRepository.load(sessions);
        assertEquals(sessionRepository.getEntities().size(), 2);
    }

    @Test(expected = AccessDeniedException.class)
    public void findByUserIdExceptionTest() {
        sessionRepository.findByUserId("qwerty");
    }

}
