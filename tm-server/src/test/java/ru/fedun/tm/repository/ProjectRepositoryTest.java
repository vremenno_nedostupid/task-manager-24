package ru.fedun.tm.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.marker.UnitServerCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class ProjectRepositoryTest {

    final ProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void init() {
        final Project project1 = new Project();
        project1.setUserId("001");
        project1.setTitle("Project 1");
        project1.setDescription("Description 1");
        projectRepository.add(project1);

        final Project project2 = new Project();
        project2.setUserId("002");
        project2.setTitle("Project 2");
        project2.setDescription("Description 2");
        projectRepository.add(project2);

        final Project project3 = new Project();
        project3.setUserId("002");
        project3.setTitle("Project 3");
        project3.setDescription("Description 3");
        projectRepository.add(project3);
    }

    @Test
    public void addTest() {
        projectRepository.clear();
        final Project project1 = new Project();
        project1.setUserId("001");
        project1.setTitle("Project 1");
        project1.setDescription("Description 1");
        projectRepository.add(project1);
        assertSame(projectRepository.getEntities().get(0), project1);
    }

    @Test
    public void removeTest() {
        final Project project = projectRepository.getEntities().get(0);
        projectRepository.remove(project.getUserId(), project);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void findAllTest() {
        assertEquals(projectRepository.findAll().size(), 3);
    }

    @Test
    public void clearTest() {
        projectRepository.clear();
        assertEquals(projectRepository.findAll().size(), 0);
    }

    @Test
    public void findOneByIdTest() {
        final Project project = projectRepository.getEntities().get(0);
        final String projectId = project.getId();
        assertEquals(projectRepository.findOneById("001", projectId), project);
    }

    @Test
    public void findOneByIndexTest() {
        final Project project = projectRepository.getEntities().get(0);
        assertEquals(projectRepository.findOneByIndex("001", 0), project);
    }

    @Test
    public void findOneByNameTest() {
        final Project project = projectRepository.getEntities().get(0);
        final String projectName = project.getTitle();
        assertEquals(projectRepository.findOneByTitle("001", projectName), project);
    }

    @Test
    public void removeOneByIdTest() {
        final Project project = projectRepository.getEntities().get(0);
        final String projectId = project.getId();
        projectRepository.removeOneById(project.getUserId(), projectId);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void removeOneByIndexTest() {
        final Project project = projectRepository.getEntities().get(0);
        projectRepository.removeOneByIndex(project.getUserId(), 0);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void removeOneByNameTest() {
        final Project project = projectRepository.getEntities().get(0);
        final String projectName = project.getTitle();
        projectRepository.removeOneByTitle(project.getUserId(), projectName);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void clearEntitiesTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        projectRepository.clear();
        assertEquals(projectRepository.getEntities().size(), 0);
    }

    @Test
    public void addVarargsTest() {
        projectRepository.clear();
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        projectRepository.add(project1, project2, project3);
        assertEquals(projectRepository.getEntities().size(), 3);
    }

    @Test
    public void AddListTest() {
        projectRepository.clear();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectRepository.add(projects);
        assertEquals(projectRepository.getEntities().size(), 3);
    }

    @Test
    public void loadOneTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        final Project project = new Project();
        projectRepository.load(project);
        assertEquals(projectRepository.getEntities().size(), 1);
    }

    @Test
    public void loadVarargsTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectRepository.load(project1, project2);
        assertEquals(projectRepository.getEntities().size(), 2);
    }

    @Test
    public void loadListTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.load(projects);
        assertEquals(projectRepository.getEntities().size(), 2);
    }

}
