package ru.fedun.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;

public final class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-json-fx";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json (fasterxml) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FASTERXML) LOAD]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().loadJsonByFasterXml(session);
        System.out.println();
    }

}
