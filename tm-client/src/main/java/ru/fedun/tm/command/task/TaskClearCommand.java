package ru.fedun.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;

public final class TaskClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear task list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CLEAR]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getTaskEndpoint().clearTasks(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
