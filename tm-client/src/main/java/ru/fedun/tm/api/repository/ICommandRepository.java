package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommands();

}
